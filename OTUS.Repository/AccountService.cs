﻿using OTUS.Repository.Interfaces;
using System;
using System.Linq;

namespace OTUS.Repository
{
	public class AccountService : IAccountService
	{
		private IRepository<Account> _repository;

		public AccountService(IRepository<Account> repository)
		{
			_repository = repository;
		}

		public void AddAccount(Account account)
		{
			if (!ValidateAccount(account))
				return;

			_repository.Add(account);
		}

		public int CountTotal()
		{
			return _repository.GetAll().Count();
		}

		private bool ValidateAccount(Account account)
		{
			if (string.IsNullOrEmpty(account.FirstName))
				throw new FormatException("Человек не может быть без имени");

			if (string.IsNullOrEmpty(account.LastName))
				throw new FormatException("Только отвества может не быть, но не Фамилии!");

			return true;
		}

	}
}
