﻿using OTUS.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OTUS.Repository
{
	public class AccountRepository : IRepository<Account>
	{
		private readonly List<Account> _accounts;

		public AccountRepository()
		{
			_accounts = new List<Account>()
			{
				new Account()
				{
					FirstName = "Федя",
					LastName = "Федоров",
					BirthDate = new DateTime(100000000)
				},
				new Account()
				{
					FirstName = "Володя",
					LastName = "Володов",
					BirthDate = new DateTime(200000000)
				},
				new Account()
				{
					FirstName = "Вася",
					LastName = "Хачатурян",
					BirthDate = new DateTime(300000000)
				}
			};
		}

		public void Add(Account item)
		{
			_accounts.Add(item);
		}

		public IEnumerable<Account> GetAll()
		{
			foreach (var account in _accounts)
			{
				yield return account;
			}
		}

		public Account GetOne(Func<Account, bool> predicate)
		{
			return _accounts.FirstOrDefault(predicate);
		}
	}
}
