﻿using System;
using System.Collections.Generic;

namespace OTUS.Repository.Interfaces
{
	public interface IRepository<T>
	{
		IEnumerable<T> GetAll();
		T GetOne(Func<T, bool> predicate);
		void Add(T item);
	}
}
