﻿using System;

namespace OTUS.Repository
{
	public class Account
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDate { get; set; }
	}
}
