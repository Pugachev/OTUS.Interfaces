﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OTUS.Repository.Interfaces;

namespace OTUS.Repository.Test
{
	[TestClass]
	public class RepositoryTest
	{
		private readonly IList<Account> _accounts = new List<Account>();
		private Mock<IRepository<Account>> _mockRepository;

		private const string FirstName = "1";
		private const string NotValidName = "";
		private const string LastName = "1";

		[TestInitialize]
		public void Initialize()
		{
			for (int i = 0; i < 3; i++)
			{
				_accounts.Add(new Account
				{
					BirthDate = new DateTime(),
					FirstName = i.ToString(),
					LastName = i.ToString(),
				});
			}

			_mockRepository  = new Mock<IRepository<Account>>();
			_mockRepository.Setup(x => x.Add(_accounts.FirstOrDefault(a => a.FirstName == FirstName)));
			_mockRepository.Setup(x => x.GetAll()).Returns(GetAccounts);
			_mockRepository.Setup(x => x.GetOne(It.Is<Func<Account, bool>>(y => true))).Returns(GetOne);
		}

		[TestCleanup]
		public void Cleanup()
		{
			_mockRepository.Reset();
		}

		[TestMethod]
		public void AddAccountUsingAccountService()
		{
			var account = _accounts.FirstOrDefault(a => a.FirstName == FirstName);
			var service = new AccountService(_mockRepository.Object);

			service.AddAccount(account);

			Assert.AreEqual(service.CountTotal(), 3);
		}

		[TestMethod]
		public void AddNoNameAccountUsingAccountService()
		{
			var account = new Account() { FirstName = NotValidName };
			var service = new AccountService(_mockRepository.Object);

			Assert.ThrowsException<FormatException>(() => service.AddAccount(account));
		}

		private IEnumerable<Account> GetAccounts()
		{
			return _accounts;
			/*new List<Account>()
		{ 
			new Account()
			{
				FirstName = "Федя",
				LastName = "Федоров",
				BirthDate = new DateTime(100000000)
			}
		};*/
		}

		private Account GetOne()
		{
			return _accounts.FirstOrDefault(x => x.FirstName == FirstName);
		}
	}
}
