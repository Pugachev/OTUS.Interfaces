﻿using System.IO;

namespace OTUS.Serializer.Interfaces
{
	public interface ISerializer<T>
	{
		Stream Serialize(T item);
		T Deserialize(Stream stream);
	}
}
