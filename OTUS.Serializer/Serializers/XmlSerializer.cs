﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using OTUS.Serializer.Interfaces;
using System.IO;
using System.Text;
using System.Xml;

namespace OTUS.Serializer.Serializers
{
	public class XmlSerializer<T> : ISerializer<T>
	{
		private IExtendedXmlSerializer _serializer;

		public XmlSerializer()
		{
			_serializer = new ConfigurationContainer()
				.UseAutoFormatting()
				.UseOptimizedNamespaces()
				.EnableImplicitTyping(typeof(T))
				.Create();
		}

		public T Deserialize(Stream stream)
		{
			T deserialized;

			using (var streamReader = new StreamReader(stream))
			{
				string contents = streamReader.ReadToEnd();
				using (MemoryStream contentStream = new MemoryStream(Encoding.UTF8.GetBytes(contents)))
				{
					using (XmlReader reader = XmlReader.Create(contentStream))
					{
						deserialized = (T)_serializer.Deserialize(reader);
					}
				}
			}

			return deserialized;
		}

		public Stream Serialize(T item)
		{
			MemoryStream stream = new MemoryStream();
			using (XmlWriter writer = XmlWriter.Create(stream))
			{
				_serializer.Serialize(writer, item);
				writer.Flush();
			}
			stream.Seek(0, SeekOrigin.Begin);
			
			return stream;
		}
	}
}
