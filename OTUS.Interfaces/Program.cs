﻿using OTUS.Interfaces.DTO;
using OTUS.Interfaces.Reader;
using OTUS.Serializer.Serializers;
using System;
using System.Collections.Generic;

namespace OTUS.Interfaces
{
	class Program
	{
		static void Main(string[] args)
		{
			var serializer = new XmlSerializer<Person>();

			var person = new Person("Oleg" , 6, 48 );

			var stream = serializer.Serialize(person);

			var restoredPerson = serializer.Deserialize(stream);

			Console.WriteLine($"Before serialization: Name {person.Name}, Age {person.Age} , Foot size {person.FootSize}.");
			Console.WriteLine($"After serialization: Name {restoredPerson.Name}, Age {restoredPerson.Age} , Foot size {restoredPerson.FootSize}.");


			List<Person> list = new List<Person>();

			for (int i = 0; i < 3; i++)
				list.Add(new Person($"Петр {i}", i, 40 + i));

			var listSerializer = new XmlSerializer<List<Person>>();
			var streamList = listSerializer.Serialize(list);

			StreamReader<List<Person>, Person> reader = new StreamReader<List<Person>, Person>(streamList, listSerializer);
			foreach (var item in reader)
			{
				Console.WriteLine($"Enumerator serialization: Name {item.Name}, Age {item.Age} , Foot size {item.FootSize}.");
			}

			Console.ReadLine();

		}
	}
}