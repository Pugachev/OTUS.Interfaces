﻿using OTUS.Interfaces.DTO;
using OTUS.Interfaces.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace OTUS.Interfaces.Sorter
{
	class PersonSorter : ISorter<Person>
	{
		public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
		{
			return notSortedItems.OrderBy(p => p.FootSize);
		}
	}
}
