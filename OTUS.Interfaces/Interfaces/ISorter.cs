﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.Interfaces.Interfaces
{
	interface ISorter<T>
	{
		IEnumerable<T> Sort(IEnumerable<T> notSortedItems);

	}
}
