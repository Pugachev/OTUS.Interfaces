﻿namespace OTUS.Interfaces.DTO
{
	class Person
	{

		public Person()
		{

		}

		public Person(
			string name, 
			int age,
			int footSize)
		{
			Name = name;
			Age = age;
			FootSize = footSize;
		}

		public string Name { get; set; }
		public int Age { get; set; }
		public int FootSize { get; set; }
	}
}
