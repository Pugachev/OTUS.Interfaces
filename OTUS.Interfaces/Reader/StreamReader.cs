﻿using OTUS.Serializer.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace OTUS.Interfaces.Reader
{
	class StreamReader<T, J> : IEnumerable<J>, IDisposable
		where T: IList<J>
	{
		private Stream _stream;
		private ISerializer<T> _serializer;

		public StreamReader(
			Stream stream,
			ISerializer<T> serializer)
		{
			_stream = stream;
			_serializer = serializer;
		}

		public IEnumerator<J> GetEnumerator()
		{
			var deserialized = _serializer.Deserialize(_stream);
			

			for (int i = 0; i < deserialized.Count; i++)
			{
				yield return deserialized[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Dispose()
		{
			_stream.Dispose();
		}
	}
}
